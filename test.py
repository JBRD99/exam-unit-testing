import functions
import unittest

class TestAdd(unittest.TestCase):

    def test_equal(self):
        self.assertEqual(functions.Add(2.0,2.0),4)

    def test_not_equal(self):
        self.assertNotEqual(functions.Add(2.0,2.0),8)

    def test_type(self):
        self.assertEqual(type(functions.Add(2.5,2.5)),int)

class TestConcatenate(unittest.TestCase):

    def test_equal(self):
        self.assertEqual(functions.Concatenate("abc","def"),"abcdef")

    def test_not_equal(self):
        self.assertNotEqual(functions.Concatenate("abc","def"),"defabc")

if __name__ == '__main__':
    unittest.main()
